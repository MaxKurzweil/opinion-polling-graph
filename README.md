# Opinion polling LOESS graph
Download:

- [R](https://ftp.fau.de/cran/)
- [RStudio](https://rstudio.com/products/rstudio/download/#download)
- "ggplot.R" and "CAT.csv" from here to your working directory

Create an R project in the working directory.

In RStudio, run these commands to install packages:

```
> install.packages("tidyverse")
> install.packages("backports")
> install.packages("anytime")
> install.packages("svglite")
```

Use "CAT.csv" to add polling data. Then run "ggplot.R" to create graph, which will be saved as "polls.svg".
